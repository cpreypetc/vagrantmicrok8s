# Vagrant + Ubuntu MicroK8s = ❤

I've had a long love affair with Vagrant. When the INFR Services guys say "too busy - how bout next week?" or the InfoSec ban-hammer comes down - I reach for Vagrant. People have been having a lot of luck with minikube...awesome project...here's an alternative using Ubuntu's newly released Microk8s even if you're not an Ubuntu user.

Most of the work has been done for you so you can get your mitts on the microK8s goodness without a bunch of hub-bub.

## What you'll need

- Virtualbox
- Vagrant
- Internet connection (for installation)
- a capable system

## Vagrant up

If you're already reading this, you're likely in the right directory. You should see a file called `Vagrantfile`.

```shell
cprey@laptop:~/vagrant_plain$ vagrant up

Bringing machine 'default' up with 'virtualbox' provider...
==> default: Importing base box 'bento/ubuntu-18.04'...
==> default: Matching MAC address for NAT networking...
==> default: Checking if box 'bento/ubuntu-18.04' is up to date...
==> default: Setting the name of the VM: vagrant_plain_default_1569696916587_79773
==> default: Clearing any previously set network interfaces...
==> default: Preparing network interfaces based on configuration...
go get a coffee...
default: done
default: 2019-09-28T20:46:19Z INFO Waiting for restart...
default: microk8s v1.16.0 from Canonical* installed
```

...more stuff streams by and then the guest is ready  

log in to your vagrant guest using the command `vagrant ssh`

from this point on if you see the `vagrant@vagrant` prompt, you will know you're on the Virtualbox guest.

the `microk8s snap` has already been installed on your guest. Ensure everything is hunky-dory

```shell
vagrant@vagrant:~$ microk8s.status
microk8s is running
addons:
knative: disabled
jaeger: disabled
fluentd: disabled
gpu: disabled
cilium: disabled
storage: disabled
registry: disabled
rbac: disabled
ingress: disabled
dns: disabled
metrics-server: disabled
linkerd: disabled
prometheus: disabled
istio: disabled
dashboard: disabled
```

Now it's time for microk8s to shine - let's enable some add-ons.

```shell
vagrant@vagrant:~$ microk8s.enable dns
Enabling DNS
Applying manifest
serviceaccount/coredns created
configmap/coredns created
deployment.apps/coredns created
service/kube-dns created
clusterrole.rbac.authorization.k8s.io/coredns created
clusterrolebinding.rbac.authorization.k8s.io/coredns created
Restarting kubelet
DNS is enabled
```

and then the Kubernetes dashboard

```shell
vagrant@vagrant:~$ microk8s.enable dashboard 
Applying manifest
serviceaccount/kubernetes-dashboard created
service/kubernetes-dashboard created
secret/kubernetes-dashboard-certs created
secret/kubernetes-dashboard-csr
...bunch more output
```

if this fails...run that same command again.

## Getting the admin token

here's the only tricky part. The directions on how to get the admin token scrolled by pretty quickly so you may have missed them.

```shell
token=$(microk8s.kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
microk8s.kubectl -n kube-system describe secret $token
```

when I ran this...I got...

```shell
vagrant@vagrant:~$ token=$(microk8s.kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
vagrant@vagrant:~$ microk8s.kubectl -n kube-system describe secret $token
Name:         default-token-rtsvp
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: default
              kubernetes.io/service-account.uid: 45bb5765-fda1-4d40-aa84-749365b3a97a

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1107 bytes
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IjN5S0o4MlA2MFdtM2FYakk5a2YtMUpuSWpzZDBVdmJyeExfZ0VzeWlFMHMifQ.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkZWZhdWx0LXRva2VuLXJ0c3ZwIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImRlZmF1bHQiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI0NWJiNTc2NS1mZGExLTRkNDAtYWE4NC03NDkzNjViM2E5N2EiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06ZGVmYXVsdCJ9.DeMWio3j5FWjhz7qSXgFD9owC4wGHQPF8qpUkxjOJ9amCVXbbKAYPEq61CGKRs7TC9-yXKz3d-_lRPfJcEAQ2sYwAGngq1yl9RtmeNGe_cdUKF5On6j-OqUd9TGj9xSYmjb0ndtkiC7ZjbUAXBhy2st77oQV8uZt8BpvbzTtGJVvQpSziS2j4RAfZBItsAQkh1cyOrzKLM8Js9vVfCCYDdx3chWNM0Ng7lv5Lnp64rrUJCbjpTElXKcKYJ-JRc2O1BwBoa6DzAbCAnXXCdiWhXY4haf9dHjf9M9AJAJDKvJYdAJ27r6FCedE6qFTcMx1Y3VW58YdXFy74ktOgc2jVQ
```

next is to start a proxy that will allow the NAT in the Vagrantfile to connect to your microk8s dashboard

```shell
vagrant@vagrant:~$ microk8s.kubectl proxy --address=0.0.0.0
```

then you can log in to the dashboard using...the big...long token, yes the whole thing. The URL to use is -

[http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy](http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy)

select `token` and paste that big block

![kubernetes dashboard](./imgs/k8sdashboard.png)

when you're logged in...
![kubernetes dashboard](./imgs/k8sdashboard2.png)

when you're done with Vagrant...you can `vagrant halt` to stop your Virtualbox microk8s box or `vagrant destroy` to delete it.

## MicroK8s 101

`microk8s.kubectl` is how to run kubectl
so `microk8s.kubectl get nodes` will show your nodes and `microk8s.kubectl get pods` will show your pods.

`microk8s.status` shows the microk8s status

`microk8s.enable` is used to enable features like dashboard RBAC etc.
